Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: X2Go Thin Client Environment
Upstream-Contact: X2Go Developers <x2go-dev@lists.x2go.org>
Source: https://code.x2go.org/gitweb?p=x2gothinclient.git;a=summary

Files: ChangeLog
 README.howto
 README.i18n
 README.qemu
 VERSION
 cdmanager/init/x2gothinclient-cdmanager.service
 chroot/init/x2gothinclient-chroot.init
 chroot/patchsets/alsa-utils.wheezy/alsa-utils
 chroot/patchsets/alsa-utils.wheezy/utils.sh
 common/etc/freerdp/known_hosts
 common/etc/freerdp/known_hosts2
 displaymanager/init/x2gothinclient-displaymanager.service
 management/README
 management/README.hostname
 management/share/etc/x2gothinclient_sessions
 management/share/tftpboot/default.cfg
 management/share/tftpboot/local-boot.cfg
 management/share/tftpboot/memtest.cfg
 management/share/tftpboot/x2go-tce.cfg
 minidesktop/desktop/x2gothinclient-minidesktop.desktop
 minidesktop/desktop/x2gothinclient-x-www-browser.desktop
 minidesktop/etc/lightdm.conf
 minidesktop/etc/restart.lightdm
 minidesktop/mate-panel-layout/x2gothinclient-minidesktop.layout
 minidesktop/schema-overrides/19_x2gothinclient-minidesktop.gschema.override
Copyright: 2005-2019, X2Go Developers (https://wiki.x2go.org)
  2010-2019, Oleksandr Shneyder <o.shneyder@phoca-gmbh.de>
  2010-2019, Moritz 'Morty' Struebe <Moritz.Struebe@informatik.uni-erlangen.de>
  2010-2019, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-2+
Comment:
 Listing main upstream contributors for file without evident
 copyright information as copyright holders. License as found in
 COPYING file.

Files: management/share/etc/x2gothinclient-displaymanager_background.svg
 management/share/etc/x2gothinclient-minidesktop_background.svg
 cdmanager/powerej/eject-button.png
 management/share/tftpboot/x2go-simple-splash.png
 management/share/tftpboot/x2go-splash.png
Copyright: 2007-2019, Heinz-M. Graesing <heinz-m.graesing@obivously-nice.de
License: GPL-2+
Comment:
 Copyright holdership is not evident from any files in the upstream
 project. However, all of the X2Go artwork (also in the X2Go TCE) has
 been contributed by the project founder Heinz-Markus Graesing, so giving
 credits to him here. X2Go artwork has always been published by Heinz under
 GPL-2+ (personal knowledge [Mike Gabriel]).

Files: cdmanager/init/x2gothinclient-cdmanager.init
 displaymanager/init/x2gothinclient-displaymanager.init
 displaymanager/sbin/x2gothinclientd
 management/etc/x2gothinclient_settings
 management/sbin/x2gothinclient_cleanup
 management/sbin/x2gothinclient_create
 management/sbin/x2gothinclient_preptftpboot
 management/sbin/x2gothinclient_shell
 management/sbin/x2gothinclient_update
 management/sbin/x2gothinclient_upgrade
 management/share/etc/x2gothinclient-displaymanager_start
 management/share/etc/x2gothinclient-minidesktop_start
 management/share/etc/x2gothinclient_init.d/005_x2gothinclient-wipe-home
 management/share/etc/x2gothinclient_init.d/010_x2gothinclient-fresh-home
 management/share/etc/x2gothinclient_init.d/950_x2gothinclient-minidesktop
Copyright: 2010-2019, X2Go project (https://wiki.x2go.org)
  2010-2019, Oleksandr Shneyder <o.shneyder@phoca-gmbh.de>
  2010-2019, Moritz 'Morty' Struebe <Moritz.Struebe@informatik.uni-erlangen.de>
  2010-2019, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-2+

Files: cdmanager/man/x2gocdmanager.8
 cdmanager/man/x2goejectcd.1
 displaymanager/man/x2gothinclientd.8
 management/man/x2gothinclient_cleanup.8
 management/man/x2gothinclient_create.8
 management/man/x2gothinclient_preptftpboot.8
 management/man/x2gothinclient_shell.8
 management/man/x2gothinclient_update.8
 management/man/x2gothinclient_upgrade.8
Copyright: 2010-2019, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-2+
Comment:
 Using man page author as copyright holder, assuming
 same license as found in COPYING file.

Files: common/lib/x2gothinclient_init
 minidesktop/ltsp/client/init/88-x2gothinclient-minidesktop_disable-ltsp-display-manager.sh
Copyright: 2007-2019, X2Go Project - https://wiki.x2go.org
  2011-2019, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
  2019, X2Go Project - https://wiki.x2go.org
  2019, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-2+

Files: cdmanager/powerej/eject.cpp
Copyright: 2005-2019, Oleksandr Shneyder
License: GPL-2+

Files: cdmanager/powerej/powerej.pro
 cdmanager/powerej/mb.cpp
 cdmanager/powerej/mb.h
 cdmanager/powerej/resources.rcc
Copyright: 2005-2019, Oleksandr Shneyder
License: GPL-2+
Comment:
 Assuming same copyright information as applicable to eject.cpp.

Files: usbmount/x2gousbmount
 cdmanager/x2gocdmanager
Copyright: 2007-2019, X2Go Project (https://wiki.x2go.org)
           2007-2019, Oleksandr Shneyder <o.shneyder@phoca-gmbh.de>
License: GPL-2+

Files: smartcardrules/21-x2gognupgccid.rules
 smartcardrules/man/x2gognupgccid.8
 smartcardrules/x2gognupgccid
 usbmount/61-x2gousbmount.rules
 usbmount/man/x2gousbmount.8
Copyright: 2007-2019, X2Go Project (https://wiki.x2go.org)
           2007-2019, Oleksandr Shneyder <o.shneyder@phoca-gmbh.de>
License: GPL-2+
Comment:
 Assuming same copyright holdership as for x2gousbmount and
 x2gocdmanager (and according to VCS information).
 .
 Assuming license as found in COPYING file.

Files: debian/*
Copyright: 2005-2009, Oleksandr Shneyder <o.shneyder@phoca-gmbh.de>
           2010-2019, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-2+

Files: debian/x2gothinclient-cdmanager.init
 debian/x2gothinclient-displaymanager.init
Copyright: 2010-2019, X2Go project (https://wiki.x2go.org)
  2010-2019, Oleksandr Shneyder <o.shneyder@phoca-gmbh.de>
  2010-2019, Moritz 'Morty' Struebe <Moritz.Struebe@informatik.uni-erlangen.de>
  2010-2022, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'
